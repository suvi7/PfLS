"""Transcript Sampler.

@author: Suvarnan Selliah
"""

from setuptools import setup, find_packages

with open("README.md", "r") as f:
    long_description = f.read()

setup(
    name='suvis_code',
    url='https://gitlab.com/suvi7/PfLS.git',
    author='Suvarnan Suvi Selliah',
    author_email='s.selliah@unibas.ch',
    description=('Input: file with gene abundance, total number of transcripts, file to write final results;'
                 'generates library (gene, number of transcripts)'),
    license='MIT',
    version='1.0.0',
    packages=find_packages(),
    install_requires=[],
    entry_points={
        'console_scripts': ['my-executable=suvis_code.cli:main'],
    },
)
