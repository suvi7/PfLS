"""Command Line Interface file.

Functions:
    main: Executes script, when the correct input is given
    __name__: Makes sure, that script is only executed, when prompted from command line
"""

import argparse
from suvis_code import TransriptSampl as ts

parser = argparse.ArgumentParser(description="Gene transcripts")
parser.add_argument("input_file", type=str, help="File with gene expression")
parser.add_argument("number_of_transcripts", type=int, help="number of total transcripts")
parser.add_argument("output_file", type=str, help="file to save final output")


def main():
    """Executes the script, when started from command line.

    Args:
        input_file(str): File with gene abundances
        number_of_transcripts(int): total number of transdcripts in sample
        output_file(str): File, where final output (generated transcripts library is saved)
    """
    args = parser.parse_args()
    avg_expression = ts.read_avg_expression(args.input_file)
    sampled_expression = ts.sample_transcripts(avg_expression, args.number_of_transcripts)
    ts.write_sample(args.output_file, sampled_expression)


if __name__ == "__main__":
    main()
