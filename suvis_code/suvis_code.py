"""Functions to sample Transcripts."""

import random


class TranscriptSampler:
    """Collection of functions for simulating single cell RNA sequencing.

    Args:
        input_file: Path to file with gene abundances.

    Attributes:
        input_file: Path to file tab-separated file with gene abundances per
                    gene.
        gene_expression: Dictionary of gene name and average expression.
                         transcripts_per_gene: Dictionary of gene name and number of
                         transcripts.
    """
    def __init__(self, file, number):
        """Class intructor."""
        self.file = file
        self.number = number

    def read_avg_expression(self, file):
        """Reads input.

        Args:
                File (str): Gene_ID & copy_nr

        Returns:
                transcripts_avg.copy() (dict): Gene_ID & averaged copy number
        """
        # open file
        with open(file, 'r') as myfile:
            # create dictionary
            transcripts_avg = {}
            transcripts_appearance = {}  # containing how many times the gene appears in the list
            # read file line after line and fill dictionary with key:value(total) pairs
            for myline in myfile:
                gene_id = ""
                nr_copies = ""
                mystring = myline
                blank_space_position = mystring.find(" ")
                # assmebling the gene_id
                for s in mystring[0:blank_space_position]:
                    gene_id = gene_id + s
                    # assembling the nr_copies
                for s in mystring[(blank_space_position + 1): len(mystring)]:
                    nr_copies = nr_copies + s
                nr_copies = int(nr_copies)
                # iterate over transcripts_avg
                # if gene_id == k, nr_copies are added to the existing number and
                # transcript_appearance.value is increased 1
                controller = False
                for (k, v) in transcripts_avg.items():
                    if gene_id == k:
                        temporary1 = v
                        transcripts_avg[gene_id] = (int(temporary1) + nr_copies)
                        temporary2 = transcripts_appearance[gene_id]
                        transcripts_appearance[gene_id] = temporary2 + 1
                        controller = True
                        # ad gene_id and nr_copies to transcripts_avg{} and transcripts_appearance
                if not controller:
                    transcripts_avg[gene_id] = nr_copies
                    transcripts_appearance[gene_id] = 1

                    # make average of nr_copies
            for (k, v) in transcripts_appearance.items():
                if v > 1:
                    temporary3 = transcripts_avg[k]
                    transcripts_avg[k] = temporary3 / v
            return transcripts_avg.copy()

    def sample_transcripts(avgs, number):
        """Transcript sample generator.

        Args:
                avgs (dict): Gene_ID & averaged copy number
                number (int): total number of transcripts

        Returns:
                output_dict.copy() (dict): Gene_ID & number of transcripts
        """
        # calculate total number of transcripts from the input dictionary
        total_input_dictionary_transcripts = 0
        for (k, v) in avgs.items():
            total_input_dictionary_transcripts += v
            # create temporaray dict with percentages of the genes
            # according to the total of transcripts from the input dict

        temporary_percentages = {}
        one_trans = 100 / total_input_dictionary_transcripts
        for (k, v) in avgs.items():
            temp1 = v * one_trans
            temporary_percentages[k] = temp1

            # create dict (gene: cp_nr) matching to the input "number"
        sample_dictionary = {}
        one_perc = number / 100
        for (k, v) in temporary_percentages.items():
            temp2 = one_perc * v
            sample_dictionary[k] = int(temp2)

            # create list with n times the gene, if gene appears 3 times, then the dict has 3 entry with that gene name
        choose_list = []
        number_of_transcript = 0
        for (k, v) in sample_dictionary.items():
            number_of_transcript = v
            while number_of_transcript > 0:
                number_of_transcript -= 1
                choose_list.append(k)

                # create randomized list, where "number" times a gene is chosen from choose_list
        randList = []
        number_copy = number
        while number_copy > 0:
            number_copy -= 1
            randList.append(random.choice(choose_list))

            # create output dict
        output_dict = {}
        for i in randList:
            switch = True
            for (k, v) in output_dict.items():
                if i == k:
                    switch = False
                    output_dict[k] = v + 1
            if switch:
                output_dict[i] = 1

                # output
        return output_dict.copy()

    def write_sample(file, sample):
        """Writes the sample to a file.

        Args:
                file (str): empty file to write on
                sample (dict): Gene_ID & number of transcripts

        Returns:
                None
        """
        # open file
        with open(file, 'w') as myfile:
            # write file
            n = 0
            for (k, v) in sample.items():
                n += 1
                temp3 = str(v)
                # first line
                if n == 1:
                    myfile.write(k + " " + temp3)
                    # subsequent lines
                else:
                    myfile.write("\n")
                    myfile.write(k + " " + temp3)
                    # output
        return
